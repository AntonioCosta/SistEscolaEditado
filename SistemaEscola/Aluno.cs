﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	public class Aluno
	{
		private int idAluno;
		private String nome;
		private String matricula;
		private DateTime dataNasc;
		private DateTime dataMatricula;

		//idAluno
		public void setIdAluno(int idAluno)
		{
			this.idAluno = idAluno;
		}
		public int getIdAluno()
		{
			return idAluno;
		}
		//nome
		public void setNome(String nome)
		{
			this.nome = nome;
		}
		public String getNome()
		{
			return nome;
		}
		//matricula
		public void setMatricula(String matricula)
		{
			this.matricula = matricula;
		}
		public String getMatricula()
		{
			return matricula;
		}
		//dataNasc
		public void setDataNasc(DateTime dataNasc)
		{
			this.dataNasc = dataNasc;
		}
		public DateTime getDataNasc()
		{
			return dataNasc;
		}
		//dataMatricula
		public void setDataMatricula(DateTime dataMatricula)
		{
			this.dataMatricula = dataMatricula;
		}
		public DateTime getDataMatricula()
		{
			return dataMatricula;
		}
	}
}
