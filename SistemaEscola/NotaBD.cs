﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SistemaEscola
{
	class NotaBD
	{
		public int inserirNota(Nota nota)
		{
			/*Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();

			NpgsqlCommand cmdTest = new NpgsqlCommand("select id_aluno, id_prova from aluno, prova where nome ='" + nota.getAluno().getNome() + "'", conn);

			NpgsqlCommand cmd = new NpgsqlCommand("insert into nota(id_aluno, id_prova from aluno, valor_nota) " +
				"values ('" + nota.getAluno().getIdAluno() + "','" + nota.getProva().getIdProva() + "','" + nota.getNota() + "')", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;*/

			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			//int n = nota.getAluno().getIdAluno();
			NpgsqlCommand cmd = new NpgsqlCommand("insert into nota(id_aluno, id_prova, valor_nota) " +
				"values ('" + nota.getAluno().getIdAluno() + "','" + nota.getProva().getIdProva() + "','" + nota.getNota() + "')", conn);

			int result = cmd.ExecuteNonQuery();
			c.desconecta();

			return result;
		}

		public List<Nota> buscarNotasPorNome(String nome)
		{
			
			Conexão c = new Conexão();
			NpgsqlConnection conn = c.conecta();
			NpgsqlCommand cmd = new NpgsqlCommand("select * from aluno where nome like '%" + nome + "%'", conn);
			NpgsqlDataReader dRead = cmd.ExecuteReader();
			List<Nota> notas = new List<Nota>();

			while (dRead.Read())
			{
				Aluno a = new Aluno();
				Prova p = new Prova();
				Nota n = new Nota();
				n.setAluno(a);
				n.setProva(p);
				n.setNota(Convert.ToDouble(dRead[2]));
				
				notas.Add(n);
			}
			c.desconecta();
			return notas;
		}
	}
}
